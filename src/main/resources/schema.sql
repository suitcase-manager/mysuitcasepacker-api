DROP TABLE IF EXISTS `item_property`;
DROP TABLE IF EXISTS `trip_items_list`;
DROP TABLE IF EXISTS `item`;
DROP TABLE IF EXISTS `property`;
DROP TABLE IF EXISTS `trip_information`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
);

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cathegory` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `item_property` (
  `item_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`property_id`),
  KEY `FK_item_property_property` (`property_id`),
  CONSTRAINT `FK_item_property_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `FK_item_property_property` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`)
);

CREATE TABLE IF NOT EXISTS `trip_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `destination_city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `packing_list_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_trip_information_user` (`user_id`),
  CONSTRAINT `FK_trip_information_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE IF NOT EXISTS `trip_items_list` (
  `packing_list_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `FK_item_trip_information_list` (`packing_list_id`),
  KEY `FK_trip_information_items` (`item_id`),
  KEY `FK_trip_items_list_user` (`user_id`),
  CONSTRAINT `FK_item_trip_information_list` FOREIGN KEY (`packing_list_id`) REFERENCES `trip_information` (`id`),
  CONSTRAINT `FK_trip_information_items` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `FK_trip_items_list_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);


