package com.mysuitcasepacker.controller;


import com.mysuitcasepacker.model.*;
import com.mysuitcasepacker.repository.Suitcaserepository;
import com.mysuitcasepacker.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class Suitcasecontroller {


    @Autowired
    private Suitcaserepository suitcaserepository;

    @Autowired
    private UserRepository userRepository;


    @PostMapping("/addnewitem")
    public void addItem (@RequestParam("categoryName") String categoryName, @RequestParam("itemTitle") String itemTitle) {
        suitcaserepository.addItem(categoryName, itemTitle);
    }

    @GetMapping("/cathegories")
    public List<Cathegory> getCathegories() {
        return suitcaserepository.fetchCathegories();
    }

    @GetMapping("/items")
    public List<Item> getItems() {
        return suitcaserepository.fetchItems();
    }

    @GetMapping("/cathegorieswithitems")
    public List<Cathegory> getCathegoriesWithItems() {
        List<Cathegory> cathegories = suitcaserepository.fetchCathegories();
        for (Cathegory cathegory : cathegories) {
            List<Item> itemsforcathegory = suitcaserepository.fetchItemsByCathegory(cathegory.getName());
            for(Item x : itemsforcathegory) {
                List<Property> itemProperties = suitcaserepository.fetchItemProperties(x.getId());
                x.setPropertyList(itemProperties);
            }
            cathegory.setItemList(itemsforcathegory);
        }
        return cathegories;
    }

    @GetMapping("/tripinformation")
    public List<TripInformation> getTripInformation() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User currentauthenticatedUser = userRepository.getUserByUsername(username);
        return suitcaserepository.loadTripInformation((currentauthenticatedUser.getId()));
    }

    @PostMapping("/tripinformation")
    public void addTripInformation(@RequestBody TripInformation x) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User currentauthenticatedUser = userRepository.getUserByUsername(username);
        suitcaserepository.addTripInformation(currentauthenticatedUser.getId(), x);

    }


    @PutMapping("/tripinformation")
    public void updateTripInformation(@RequestBody TripInformation y) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User currentauthenticatedUser = userRepository.getUserByUsername(username);
        suitcaserepository.updateTripInformation(currentauthenticatedUser.getId(), y);
    }

    @DeleteMapping("/tripinformation")
    public void deleteTripInformation(@RequestParam("id")int id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User currentauthenticatedUser = userRepository.getUserByUsername(username);
        suitcaserepository.deleteTripInformation(currentauthenticatedUser.getId(), id);
    }

}