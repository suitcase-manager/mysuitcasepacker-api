package com.mysuitcasepacker.controller;


import com.mysuitcasepacker.dto.GenericResponseDto;
import com.mysuitcasepacker.dto.JwtRequestDto;
import com.mysuitcasepacker.dto.JwtResponseDto;
import com.mysuitcasepacker.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.mysuitcasepacker.service.UserService;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}
