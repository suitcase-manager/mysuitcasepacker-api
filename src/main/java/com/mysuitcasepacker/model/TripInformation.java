package com.mysuitcasepacker.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class TripInformation {
    private  int id;
    private int userId;
    private String destinationCity;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String packingListName;
    private List<Integer> selectedItems;


    public TripInformation() {
    }

    public TripInformation(int id, int userId, String destinationCity, LocalDate fromDate, LocalDate toDate, String packingListName) {
        this.id = id;
        this.userId = userId;
        this.destinationCity = destinationCity;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.packingListName = packingListName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getPackingListName() {
        return packingListName;
    }

    public void setPackingListName(String packingListName) {
        this.packingListName = packingListName;
    }

    public List<Integer> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<Integer> selectedItems) {
        this.selectedItems = selectedItems;
    }
}
