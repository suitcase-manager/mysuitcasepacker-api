package com.mysuitcasepacker.repository;

import com.mysuitcasepacker.model.*;
import jdk.jfr.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class Suitcaserepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addItem(String cathegoryName, String itemTitle) {
        jdbcTemplate.update("insert into `item` (`cathegory`, `title`) values (?, ?)", cathegoryName, itemTitle);
    }

    public List<TripInformation> loadTripInformation(int userId) {
        List<TripInformation> tripInfoWithItems = jdbcTemplate.query("SELECT * from trip_information where user_id =?", new Object[]{userId}, (row, rowNum) -> {
                    return new TripInformation(
                            row.getInt("id"),
                            row.getInt("user_id"),
                            row.getString("destination_city"),
                            row.getDate("from_date").toLocalDate(),
                            row.getDate("to_date").toLocalDate(),
                            row.getString("packing_list_name")
                    );
                }
        );

        for (TripInformation tripInfo : tripInfoWithItems) {
            List<Integer> tripItems = jdbcTemplate.queryForList(
                    "SELECT item.id " +
                            "FROM item " +
                            "INNER JOIN trip_items_list ON trip_items_list.item_id = item.id " +
                            "WHERE trip_items_list.packing_list_id = ?",
                    new Object[]{tripInfo.getId()}, Integer.class);
            tripInfo.setSelectedItems(tripItems);
        }
        return tripInfoWithItems;
    }


    public List<Cathegory> fetchCathegories() {
        List<Cathegory> cathegories = jdbcTemplate.query("SELECT distinct(cathegory) FROM item ", (row, rowNum) -> {
                    return new Cathegory(
                            row.getString("cathegory")
                    );
                }
        );
        return cathegories;
    }


    public List<Item> fetchItems() {
        List<Item> items = jdbcTemplate.query("SELECT * FROM item", (row, rowNum) -> {
                    return new Item(
                            row.getInt("id"),
                            row.getString("title")
                    );
                }
        );
        return items;
    }


    public List<Item> fetchItemsByCathegory(String cathegory) {
        List<Item> items = jdbcTemplate.query("SELECT * FROM item where cathegory=?", new Object[]{cathegory}, (row, rowNum) -> {
                    return new Item(
                            row.getInt("id"),
                            row.getString("title")
                    );
                }
        );
        return items;
    }


    public List<Property> fetchItemProperties(int itemId) {
        List<Property> propertyList = jdbcTemplate.query("SELECT p.* from property p INNER JOIN item_property ip ON ip.property_id= p.id WHERE ip.item_id=?", new Object[]{itemId}, (row, rowNum) -> {
                    return new Property(
                            row.getInt("id"),
                            row.getString("name")
                    );
                }
        );
        return propertyList;
    }

    public void addTripInformation(int userId, TripInformation tripInformation) {
        String destinationCity = tripInformation.getDestinationCity();
        LocalDate fromDate = tripInformation.getFromDate();
        LocalDate toDate = tripInformation.getToDate();
        String packingListName = tripInformation.getPackingListName();
        List<Integer> selectedItems = tripInformation.getSelectedItems();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(
                                    "INSERT INTO trip_information (destination_city, from_date, to_date, packing_list_name, user_id) VALUES (?,?,?,?,?)",
                                    Statement.RETURN_GENERATED_KEYS
                            );
                    ps.setString(1, destinationCity);
                    ps.setDate(2, java.sql.Date.valueOf(fromDate));
                    ps.setDate(3, java.sql.Date.valueOf(toDate));
                    ps.setString(4, packingListName);
                    ps.setInt(5, userId);
                    return ps;
                },
                keyHolder
        );
        int tripInformationId = keyHolder.getKey().intValue();

        for (int a : selectedItems) {
            jdbcTemplate.update("INSERT INTO trip_items_list (item_id, packing_list_id, user_id) VALUES (?,?,?)", a, tripInformationId, userId);
        }
    }


    public void updateTripInformation(int userId, TripInformation tripInformation) {
        String destinationCity = tripInformation.getDestinationCity();
        LocalDate fromDate = tripInformation.getFromDate();
        LocalDate toDate = tripInformation.getToDate();
        String packingListName = tripInformation.getPackingListName();
        jdbcTemplate.update(
                "UPDATE trip_information SET destination_city = ?, from_date =?, to_date= ?, packing_list_name = ?, user_id = ? WHERE id = ?",
                tripInformation.getDestinationCity(), tripInformation.getFromDate(), tripInformation.getToDate(), tripInformation.getPackingListName(), tripInformation.getUserId(), tripInformation.getId());
    }

    public void deleteTripInformation(int userId, int id) {
        jdbcTemplate.update("delete from trip_items_list where packing_list_id = ? and user_id = ?", id, userId);
        jdbcTemplate.update("delete from trip_information where id = ? and user_id = ?", id, userId);
    }


}
